
<!-- Footer -->
<footer class="footer">

    &copy; 2015 <strong>Neon</strong> Admin Theme by <a href="http://laborator.co" target="_blank">Laborator</a>

</footer>
</div>

<!-- End page container -->
</div>



<!-- Imported styles on this page -->
<link rel="stylesheet" href="assets/css/jquery.selectBoxIt.css">

<!-- Imported styles on this page -->
<link rel="stylesheet" href="assets/css/jquery-jvectormap-1.2.2.css">
<link rel="stylesheet" href="assets/css/rickshaw.min.css">

<!-- Bottom scripts (common) -->
<script src="assets/js/TweenMax.min.js"></script>
<script src="assets/js/jquery-ui-1.10.3.minimal.min.js"></script>
<script src="assets/js/bootstrap.js"></script>
<script src="assets/js/joinable.js"></script>
<script src="assets/js/resizeable.js"></script>
<script src="assets/js/neon-api.js"></script>
<script src="assets/js/jquery-jvectormap-1.2.2.min.js"></script>


<!-- Imported scripts on this page -->
<script src="assets/js/jquery.bootstrap.wizard.min.js"></script>
<script src="assets/js/jquery.validate.min.js"></script>
<script src="assets/js/jquery.inputmask.bundle.js"></script>
<script src="assets/js/jquery.selectBoxIt.min.js"></script>
<script src="assets/js/bootstrap-datepicker.js"></script>
<script src="assets/js/bootstrap-switch.min.js"></script>
<script src="assets/js/jquery.multi-select.js"></script>
<script src="assets/js/jquery-jvectormap-europe-merc-en.js"></script>
<script src="assets/js/jquery.sparkline.min.js"></script>
<script src="assets/js/d3.v3.js"></script>
<script src="assets/js/rickshaw.min.js"></script>
<script src="assets/js/raphael-min.js"></script>
<script src="assets/js/morris.min.js"></script>
<script src="assets/js/toastr.js"></script>
<script src="assets/js/fullcalendar.min.js"></script>

<!-- Imported scripts on this page -->
<script src="assets/js/datatables.js"></script>
<script src="assets/js/select2.min.js"></script>

<!-- JavaScripts initializations and stuff -->
<script src="assets/js/neon-custom.js"></script>


<!-- Demo Settings -->
<script src="assets/js/neon-demo.js"></script>

</body>
</html>