<?php

include('header.php');
include('sidebar.php');
?>


    <div class="main-content">

<?php

include('navbar.php');

?>

    <h1>عرض الملف الشخصى</h1>
<hr>
<div class="row">
    <!-- left column -->
    <div class="col-md-3">
        <div class="text-center">
            <img src="assets/images/profile-picture.png" class="avatar img-circle" alt="avatar">

        </div>
    </div>

    <!-- edit form column -->
    <div class="col-md-9 personal-info">
        <div class="alert alert-info alert-dismissable">
            <a class="panel-close close" data-dismiss="alert">×</a>
            <i class="fa fa-coffee"></i>
            This is an <strong>.alert</strong>. Use this to show important messages to the user.
        </div>
        <h3>المعلومات الشخصية</h3>

        <form class="form-horizontal" role="form">
            <div class="form-group">
                <label class="col-lg-3 control-label">الاسم بالكامل :</label>
                <div class="col-lg-8">
                    <p class="form-control-static">احمد </p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">اسم المستخدم :</label>
                <div class="col-md-8">
                    <p class="form-control-static">احمد</p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label">الشركة :</label>
                <div class="col-lg-8">
                    <p class="form-control-static">احمد</p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label">الاميل :</label>
                <div class="col-lg-8">
                    <p class="form-control-static">mkn@yahoo.com</p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"></label>
                <div class="col-md-8">
                    <button type="button" class="btn btn-primary">حفظ التغيرات</button>

                </div>
            </div>
        </form>
    </div>
</div>

<?php

include('footer.php');

?>