<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />

    <link rel="icon" href="assets/images/favicon.ico">

    <title>Neon | Login</title>

    <link rel="stylesheet" href="assets/css/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/neon-rtl.css">
    <link rel="stylesheet" href="assets/css/custom.css">

    <script src="assets/js/jquery-1.11.3.min.js"></script>

    <!--[if lt IE 9]>
    <script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>


<body class="page-body login-page login-form-fall" data-url="http://neon.dev">
<!-- This is needed when you send requests via Ajax -->
<script type="text/javascript">
    var baseurl = '';
</script>

<div class="login-container">

    <div class="login-header login-caret">

        <div class="login-content">

            <a href="index.html" class="logo">
                <img src="assets/images/logo@2x.png" width="120" alt="" />
            </a>

            <p class="description">ادخل بريدك الالكترونى وسيتم ارسال رابط استعادة كلمة المرور.</p>

        </div>
    </div>


    <div class="form">

        <div class="login-content">

            <form method="post" role="form" id="form_forgot_password">

                <div class="form-forgotpassword-success">
                    <i class="entypo-check"></i>
                    <h3>Reset email has been sent.</h3>
                    <p>Please check your email, reset password link will expire in 24 hours.</p>
                </div>

                <div class="form-steps">

                    <div class="step current" id="step-1">

                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="entypo-mail"></i>
                                </div>

                                <input type="text" class="form-control" name="email" id="email" placeholder="البريد الالكترونى" data-mask="email" autocomplete="off" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="entypo-palette"></i>
                                </div>

                                <input type="password" class="form-control" name="pass" id="pass" placeholder="كلمة المرور" data-mask="password" autocomplete="off" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="entypo-palette"></i>
                                </div>

                                <input type="password" class="form-control" name="pass1" id="passone" placeholder="تاكيد كلمة المرور" data-mask="password" autocomplete="off" />
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-info btn-block btn-login">
                                ارسال الطلب
                                <i class="entypo-right-open-mini"></i>
                            </button>
                        </div>

                    </div>

                </div>

            </form>


            <div class="login-bottom-links">

                <a href="extra-login.html" class="link">
                    <i class="entypo-lock"></i>
                    الرجوع لصفحة تسجيل الدخول
                </a>

                <br />

                <a href="#">ToS</a>  - <a href="#">Privacy Policy</a>
            </div>
        </div>
    </div>
</div>

<!-- Bottom scripts (common) -->
<!--<script src="assets/js/TweenMax.min.js"></script>-->
<script src="assets/js/jquery-ui-1.10.3.minimal.min.js"></script>
<script src="assets/js/bootstrap.js"></script>


</body>
</html>