<?php
include ("header.php");
include ("sidebar.php");
?>
<div class="main-content">
<?php include ("navbar.php");?>
<h1>Register</h1>
		<hr>
		<div class="row">
		  <!-- left column -->
		  <div class="col-md-3">
			<div class="text-center">
			  <img src="assets/images/avatar.png" class="avatar img-circle" alt="avatar">			  
			</div>
		  </div>
		  
		  <!-- edit form column -->
		  <div class="col-md-9 personal-info">
			
			<h3>Personal info</h3>
			
			<form class="form-horizontal" role="form">
			  <div class="form-group">
				<label class="col-lg-3 control-label">full name:</label>
				<div class="col-lg-8">
				  <input class="form-control" type="text" value="">
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-md-3 control-label">Username:</label>
				<div class="col-md-8">
				  <input class="form-control" type="text" value="">
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-md-3 control-label">Upload photo:</label>
				<div class="col-md-8">
				 <input type="file" class="form-control">
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-md-3 control-label">role:</label>
				<div class="col-md-8">
					<select class="selectpicker">
						<option>super_admin</option>
					  	<option>admin</option>
					  	<option>agent</option>
					 	 <option>customer</option>
					</select>
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-lg-3 control-label">Email:</label>
				<div class="col-lg-8">
				  <input class="form-control" type="text" value="">
				</div>
			  </div>
			   <div class="form-group">
				<label class="col-md-3 control-label">Password:</label>
				<div class="col-md-8">
				  <input class="form-control" type="password" value="">
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-md-3 control-label">Confirm password:</label>
				<div class="col-md-8">
				  <input class="form-control" type="password" value="">
				</div>
			  </div>		 
			  <div class="form-group">
				<label class="col-md-3 control-label"></label>
				<div class="col-md-8">
				  <input type="button" class="btn btn-primary" value="Save Changes">
				  <span></span>
				  <input type="reset" class="btn btn-default" value="Cancel">
				</div>
			  </div>
			</form>
		  </div>
	  </div>
		


<?php include ("footer.php");?>

		