<?php
include ("header.php");
include ("sidebar.php");
?>
<div class="main-content">
<?php include ("navbar.php");?>

		<div id="table-2_wrapper" class="dataTables_wrapper no-footer">
			<table class="table table-bordered table-striped datatable" id="table-2">
				<thead>
					<tr>
						<th>
							<div class="checkbox checkbox-replace">
								<input type="checkbox" id="chk-1">
							</div>
						</th>
						<th>User name</th>
						<th>id</th>						
						<th>full name </th>
						<th>Email</th>
						<th>Role</th>
						<th>Action</th>
					</tr>
				</thead>
				
				<tbody>
				
					<tr>
						<td>
							<div class="checkbox checkbox-replace">
								<input type="checkbox" id="chk-1">
							</div>
						</td>
						<td>mohamed</td>
						<td>#1</td>						
						<td>mohamed  ahmed</td>
						<td>mohamed@nas.com</td>
						<td>admin</td>
						<td>
							<a href="#" class="btn btn-default btn-sm btn-icon icon-left">
								<i class="entypo-pencil"></i>
								Edit
							</a>
							
							<form class="inline-form">
								<button href="#" class="btn btn-primary">
								<i class="entypo-cancel"></i>
								Delete
								</button>
							</form>
							
							
						</td>
					</tr>
					
					<tr>
						<td>
							<div class="checkbox checkbox-replace">
								<input type="checkbox" id="chk-1">
							</div>
						</td>
						<td>mohamed</td>
						<td>#1</td>						
						<td>mohamed  ahmed</td>
						<td>mohamed@nas.com</td>
						<td>admin</td>
						<td>
							<a href="#" class="btn btn-default btn-sm btn-icon icon-left">
								<i class="entypo-pencil"></i>
								Edit
							</a>
							
							<form class="inline-form">
								<button href="#" class="btn btn-primary">
								<i class="entypo-cancel"></i>
								Delete
								</button>
							</form>
							
						</td>
					</tr>	
					
					
					<tr>
						<td>
							<div class="checkbox checkbox-replace">
								<input type="checkbox" id="chk-1">
							</div>
						</td>
						<td>mohamed</td>
						<td>#1</td>						
						<td>mohamed  ahmed</td>
						<td>mohamed@nas.com</td>
						<td>admin</td>
						<td>
							<a href="#" class="btn btn-default btn-sm btn-icon icon-left">
								<i class="entypo-pencil"></i>
								Edit
							</a>
							
							<form class="inline-form">
								<button href="#" class="btn btn-primary">
								<i class="entypo-cancel"></i>
								Delete
								</button>
							</form>
							
							
						</td>
					</tr>
					
					
					
					<tr>
						<td>
							<div class="checkbox checkbox-replace">
								<input type="checkbox" id="chk-1">
							</div>
						</td>
						<td>mohamed</td>
						<td>#1</td>						
						<td>mohamed  ahmed</td>
						<td>mohamed@nas.com</td>
						<td>admin</td>
						<td>
							<a href="#" class="btn btn-default btn-sm btn-icon icon-left">
								<i class="entypo-pencil"></i>
								Edit
							</a>
							
							<form class="inline-form">
								<button href="#" class="btn btn-primary">
								<i class="entypo-cancel"></i>
								Delete
								</button>
							</form>
							
							
						</td>
					</tr>
				
					
					<tr>
						<td>
							<div class="checkbox checkbox-replace">
								<input type="checkbox" id="chk-1">
							</div>
						</td>
						<td>mohamed</td>
						<td>#1</td>						
						<td>mohamed  ahmed</td>
						<td>mohamed@nas.com</td>
						<td>admin</td>
						<td>
							<a href="#" class="btn btn-default btn-sm btn-icon icon-left">
								<i class="entypo-pencil"></i>
								Edit
							</a>
							
							<form class="inline-form">
								<button href="#" class="btn btn-primary">
								<i class="entypo-cancel"></i>
								Delete
								</button>
							</form>
							
							
						</td>
					</tr>
					
				</tbody>
			</table>
			<div class="dataTables_info" id="table-2_info" role="status" aria-live="polite">Showing 1 to 5 of 5 entries</div>
			<div class="dataTables_paginate paging_simple_numbers" id="table-2_paginate">
				<a class="paginate_button previous disabled" aria-controls="table-2" data-dt-idx="0" tabindex="0" id="table-2_previous">Previous</a>
				<span><a class="paginate_button current" aria-controls="table-2" data-dt-idx="1" tabindex="0">1</a></span>
				<a class="paginate_button next disabled" aria-controls="table-2" data-dt-idx="2" tabindex="0" id="table-2_next">Next</a>
			</div>
		</div>
		<br />
		
		<a href="javascript: fnClickAddRow();" class="btn btn-primary">
			<i class="entypo-plus"></i>
			Add Row
		</a>
		
		
		
		<?php include ("footer.php");?>

							