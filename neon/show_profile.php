
		<?php
include ("header.php");
include ("sidebar.php");
?>
<div class="main-content">
<?php include ("navbar.php");?>
<h1>Show Profile</h1>
		<hr>
		<div class="row">
		  <!-- left column -->
		  <div class="col-md-3">
			<div class="text-center">
			  <img src="assets/images/profile-picture.png" class="avatar img-circle" alt="avatar">
			  
			</div>
		  </div>
		  
		  <!-- edit form column -->
		  <div class="col-md-9 personal-info">
			
			<h3>Personal info</h3>
			
			<form class="form-horizontal" role="form">
			  <div class="form-group">
				<label class="col-lg-3 control-label">full name:</label>
				<div class="col-lg-8">
					<p class="form-control-static">John Henderson</p>
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-md-3 control-label">Username:</label>
				<div class="col-md-8">
			  		<p class="form-control-static">John Henderson</p>
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-lg-3 control-label">Company:</label>
				<div class="col-lg-8">
				  <p class="form-control-static">NAS</p>
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-lg-3 control-label">Email:</label>
				<div class="col-lg-8">
				<p class="form-control-static">janesemail@gmail.com</p>  
				</div>
			  </div>			 
			</form>
		  </div>
	  </div>
		


<?php include ("footer.php");?>

